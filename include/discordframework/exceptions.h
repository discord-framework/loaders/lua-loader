#pragma once

#include <exception>
#include <string>

namespace DiscordFramework {

class exception : public std::exception {
protected:
  /**
   * @brief Exception message
   */
  std::string msg;

public:
  using std::exception::exception;

  /**
   * @brief Construct a new exception object
   */
  exception() = default;

  /**
   * @brief Construct a new exception object
   *
   * @param what reason message
   */
  explicit exception(const char *what) : msg(what) {}

  /**
   * @brief Construct a new exception object
   *
   * @param what reason message
   * @param len length of reason message
   */
  exception(const char *what, size_t len) : msg(what, len) {}

  /**
   * @brief Construct a new exception object
   *
   * @param what reason message
   */
  explicit exception(const std::string &what) : msg(what) {}

  /**
   * @brief Construct a new exception object
   *
   * @param what reason message
   */
  explicit exception(std::string &&what) : msg(std::move(what)) {}

  /**
   * @brief Construct a new exception object (copy constructor)
   */
  exception(const exception &) = default;

  /**
   * @brief Construct a new exception object (move constructor)
   */
  exception(exception &&) = default;

  /**
   * @brief Destroy the exception object
   */
  ~exception() override = default;

  /**
   * @brief Copy assignment operator
   *
   * @return exception& reference to self
   */
  exception &operator=(const exception &) = default;

  /**
   * @brief Move assignment operator
   *
   * @return exception& reference to self
   */
  exception &operator=(exception &&) = default;

  /**
   * @brief Get exception message
   *
   * @return const char* error message
   */
  [[nodiscard]] const char *what() const noexcept override {
    return msg.c_str();
  };
};

#ifndef _DOXYGEN_
#define derived_exception(name, ancestor)                                      \
  class name : public ancestor {                                               \
  public:                                                                      \
    using DiscordFramework::exception::exception;                              \
    name() = default;                                                          \
    explicit name(const char *what) : exception(what) {}                       \
    name(const char *what, size_t len) : exception(what, len) {}               \
    explicit name(const std::string &what) : exception(what) {}                \
    explicit name(std::string &&what) : exception(what) {}                     \
    name(const name &) = default;                                              \
    name(name &&) = default;                                                   \
    ~name() override = default;                                                \
    name &operator=(const name &) = default;                                   \
    name &operator=(name &&) = default;                                        \
    [[nodiscard]] const char *what() const noexcept override {                 \
      return msg.c_str();                                                      \
    };                                                                         \
  };
#endif

#ifdef _DOXYGEN_
/*
 * THESE DEFINITIONS ARE NOT THE REAL DEFINITIONS USED BY PROGRAM CODE.
 *
 * They exist only to cause Doxygen to emit proper documentation for the derived
 * exception types. Proper definitions are emitted by the `derived_exception`
 * macro in the "else" section.
 */

/**
 * @brief Represents an error with regex matching check.
 * @note This is a stub for documentation purposes. For full information on
 * supported methods, please see DiscordFramework::exception.
 */
class regex_exception : public DiscordFramework::exception {};

/**
 * @brief Represents an error with http request.
 * @note This is a stub for documentation purposes. For full information on
 * supported methods, please see DiscordFramework::exception.
 */
class http_exception : public DiscordFramework::exception {};

#else

derived_exception(regex_exception, DiscordFramework::exception);
derived_exception(http_exception, DiscordFramework::exception);

#endif

} // namespace DiscordFramework
