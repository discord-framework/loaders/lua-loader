#pragma once

#include <filesystem>
#include <string>

namespace DiscordFramework {
struct PluginInfo {
  std::string name;
  std::string loader;
  std::filesystem::path path;
};

namespace Plugins {

void checkIfDirectoryValid(const std::filesystem::directory_entry &entry);

PluginInfo readDirectory(const std::filesystem::path &path);

} // namespace Plugins
} // namespace DiscordFramework
