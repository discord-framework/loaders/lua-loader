#pragma once

#include <discordframework/core/bot.h>
#include <discordframework/plugins-loader/plugin_info.h>

namespace DiscordFramework {

class __attribute__((visibility("default"))) Loader {
public:
  Loader(Bot *bot);
  virtual ~Loader();

  virtual void load(PluginInfo *info);

  virtual std::string name() const { return ""; };

protected:
  Bot *m_bot;

private:
};

typedef Loader *(initfunctype)(Bot *);

/**
 * @brief A macro that lets us simply defnied the entrypoint of a plugin by name
 */
#define ENTRYPOINT(plug_class_name)                                            \
  extern "C" __attribute__((visibility("default"))) DiscordFramework::Loader * \
  init_loader(DiscordFramework::Bot *bot) {                                    \
    return new plug_class_name(bot);                                           \
  }
} // namespace DiscordFramework
