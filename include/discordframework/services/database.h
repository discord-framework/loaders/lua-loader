#pragma once

#include <discordframework/services/service.h>
#include <memory>
#include <pqxx/pqxx>

namespace DiscordFramework::Services {

class DatabaseService : public Service {
public:
  /**
   * @brief Initialize the database service
   *
   * @return std::shared_ptr<Service> The service instance
   */
  static std::shared_ptr<Service> init();
  ~DatabaseService();

  /**
   * @brief Get the Connection object
   *
   * @return pqxx::connection*
   */
  pqxx::connection *connection();

  /**
   * @brief Check if the database is enabled
   *
   * @return bool
   */
  bool isEnabled();

  /**
   * @brief Check if the database is connected
   *
   * @return bool
   */
  bool isConnected();

  /**
   * @brief Boot the database service
   */
  void boot() override;

private:
  DatabaseService();

  // The connection instance to the database
  std::shared_ptr<pqxx::connection> c;

  // The state of the database manager
  bool state = false;
};

} // namespace DiscordFramework::Services
