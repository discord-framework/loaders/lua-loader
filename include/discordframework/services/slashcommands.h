#pragma once

#include <discordframework/services/service.h>

namespace DiscordFramework {
namespace Services {

class SlashCommandsService : public Service {
public:
  SlashCommandsService();
  ~SlashCommandsService();
};

} // namespace Services
} // namespace DiscordFramework
