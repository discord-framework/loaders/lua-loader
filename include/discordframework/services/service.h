#pragma once

#include <discordframework/services/service_manager.h>
#include <memory>

namespace DiscordFramework {
class ServiceManager;

/**
 * @brief Abstract representation of a service. Need to be inherited to be able
 * to be used.
 *
 */
class Service {

public:
  /**
   * @brief Destroy the Service object
   *
   */
  virtual ~Service(){};

  /**
   * @brief Methods called before the service is destroyed
   * This can help to clean up the service before his destruction
   */
  virtual void destroy(){};

  /**
   * @brief Boot the service
   *
   */
  virtual void boot(){};

protected:
  friend class ServiceManager;
  void set_manager(ServiceManager *m_manager);
  ServiceManager *m_service_manager;
};
} // namespace DiscordFramework
