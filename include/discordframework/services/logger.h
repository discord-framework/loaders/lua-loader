#pragma once

#include <discordframework/services/service.h>

#include <spdlog/spdlog.h>

namespace DiscordFramework::Services {

class LoggerService : public Service {
public:
  /**
   * @brief Initialize the logger service
   * The logger will use an ouptput will if specified
   */
  static std::shared_ptr<Service> init();

  /**
   * @brief Get the logger instance
   *
   * @return spdlog::logger*
   */
  static spdlog::logger *instance();

  ~LoggerService() override = default;

private:
  explicit LoggerService();
};

} // namespace DiscordFramework::Services
