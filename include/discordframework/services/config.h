#pragma once

#include <discordframework/core/configuration.h>
#include <discordframework/services/service.h>

typedef nlohmann::json json;

namespace DiscordFramework::Services {

/**
 * @brief Config service
 * It will load the main configuration for the bot and proxy the configuration
 * methods from the Configuration class.
 */
class ConfigService : public Service {
public:
  /**
   * @brief Initialize the configuration service
   * The configuration will be created/loaded from the given file.
   *
   * @param path Path to the configuration file
   * @return std::shared_ptr<Service> The service instance
   */
  static std::shared_ptr<Service> init(const char *path);

  /**
   * @brief Load the configuration file (proxied from Configuration::load)
   */
  void load() { this->m_config->load(); }

  /**
   * @brief Save the configuration file (proxied from Configuration::save)
   *
   */
  void save() { this->m_config->save(); }

  /**
   * @brief Get the configuration object to work with, it will give you
   * a simple `nlohmann::json` object instance (proxied from
   * Configuration::JSON)
   *
   * @return json* The configuration object
   */
  json *JSON() { return this->m_config->JSON(); }

  /**
   * @brief Get the Env object (proxied from Configuration::getEnv)
   *
   * @tparam T
   * @param key The environment variable name to get
   * @return T
   */
  template <typename T> T getEnv(const std::string &key) {
    return this->m_config->getEnv<T>(key);
  }

  /**
   * @brief Check if the environment variable is set (proxied from
   * Configuration::hasEnv)
   *
   * @param key The environment variable name to check
   * @return bool
   */
  bool hasEnv(const std::string &key) { return this->m_config->hasEnv(key); }

  ~ConfigService() override = default;

private:
  explicit ConfigService(const char *path);

  std::shared_ptr<Configuration> m_config;
};

} // namespace DiscordFramework::Services
