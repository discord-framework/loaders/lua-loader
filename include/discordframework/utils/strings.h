#pragma once

#include <string>

namespace Helpers {

/**
 * @brief Class providing string manipulation methods.
 */
class __attribute__((visibility("default"))) Strings {
public:
  /**
   * @brief Replace all occurences from a text with something else
   * @param subject Text to transform
   * @param search Term to replace
   * @param replace Replacement term
   * @return Transformed text
   */
  static std::string replaceString(std::string subject,
                                   const std::string &search,
                                   const std::string &replace);

  /**
   * @brief Transform a string to lowercase
   * @tparam T
   * @param s String to transform
   * @return Transformed string
   */
  template <typename T>
  static std::basic_string<T> toLower(const std::basic_string<T> &s) {
    std::basic_string<T> s2 = s;
    std::transform(s2.begin(), s2.end(), s2.begin(), tolower);
    return std::move(s2);
  }

  /**
   * @brief Transform a string to uppercase
   *
   * @tparam T
   * @param s The string to transform
   * @return Transformed string
   */
  template <typename T>
  static std::basic_string<T> toUpper(const std::basic_string<T> &s) {
    std::basic_string<T> s2 = s;
    std::transform(s2.begin(), s2.end(), s2.begin(), toupper);
    return std::move(s2);
  }

  /**
   * @brief Trim from beginning of string (left)
   * @param s string to trim
   * @return trimmed string
   */
  static inline std::string ltrim(std::string s) {
    return s.erase(0, s.find_last_not_of(" \t\n\r\f\v"));
    return s;
  }

  /**
   * @brief Trim from end of string (right)
   * @param s string to trim
   * @return trimmed string
   */
  static inline std::string rtrim(std::string s) {
    s.erase(0, s.find_last_not_of(" \t\n\r\f\v"));
    return s;
  }

  /**
   * @brief Trim both ends of string (left + right)
   * @param s string to trim
   * @return trimmed string
   */
  static inline std::string trim(std::string s) { return ltrim(rtrim(s)); }
};

}; // namespace Helpers
