#pragma once

#include "../exceptions.h"
#include <exception>
#include <pcre.h>
#include <string>
#include <vector>

namespace Helpers {

/**
 * @brief Class PCRE represents a perl compatible regular expression\n
 * This is internally managed by libpcre.
 */
class __attribute__((visibility("default"))) PCRE {
  const char *pcre_error;
  int pcre_error_ofs;
  struct real_pcre8_or_16 *compiled_regex;

public:
  /**
   * @bried Build a PCRE regular expression object
   *
   * @param match regex to use for comparisions
   * @param case_insensitive if true, the comparision will be case insensitive
   */
  PCRE(const std::string &match, bool case_insensitive = false);
  ~PCRE();

  /**
   * @brief Check if a string match with the regular expression
   * @param comparision The string to compare
   * @return True if it match with the regular expression
   */
  bool match(const std::string &comparision);

  /**
   * @brief Check if a string match with the regular expression
   * @param matches List of strings to compare
   * @return True if it match with the regular expression
   */
  bool match(const std::string &comparision, std::vector<std::string> &matches);
};
} // namespace Helpers
