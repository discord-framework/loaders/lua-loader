#pragma once

#include "../exceptions.h"
#include <cpr/cpr.h>
#include <exception>
#include <string>

namespace Helpers {
/**
 * @brief Files manipulation with remote servers and local fileystem
 */
class __attribute__((visibility("default"))) Files {
public:
  /**
   * @brief Download a file from an url
   * @param url The url to fetch
   * @return File content
   */
  static std::string downloadFile(std::string url);

  /**
   * @brief Upload a file to an url
   * @param url The url to upload to
   * @param data The data to upload
   * @return std::string
   */
  static std::string uploadFile(std::string url, std::string data);
};
} // namespace Helpers
