#pragma once

#include <discordframework/services/service_manager.h>
#include <dpp/dpp.h>
#include <memory>

namespace DiscordFramework {

/**
 * @brief Intents possible to enable on the bot
 */
enum BotIntents {
  member = (1 << 0),
  message = (1 << 1),
  presence = (1 << 2),
};

/**
 * @brief The main class of the program, this is here that everything is setup
 * and run for the bot
 */
class Bot {
public:
  /// The cluster instance
  std::shared_ptr<dpp::cluster> cluster;

  /**
   * @brief Instantiates a new object
   *
   * @param serviceManager Your service manager
   * @return Bot The bot instance
   */
  static std::shared_ptr<Bot> create(ServiceManager *serviceManager);
  /**
   * @brief Starts the bot
   */
  void start();

  ~Bot();

  /**
   * @brief Set the token object
   *
   * @param token The token
   * @return Bot& The provided bot instance (to reuse it)
   */
  Bot &set_token(std::string token);
  /**
   * @brief Set the cluster configuration
   *
   * @param clusterID The cluster ID
   * @param maxClusters The maximum number of clusters
   * @return Bot& The provided bot instance (to reuse it)
   */
  Bot &set_cluster(int clusterID, int maxClusters);
  /**
   * @brief Set the intents
   *
   * @param intents The intents
   * @return Bot& The provided bot instance (to reuse it)
   */
  Bot &enable_intents(uint8_t intents);

  /**
   * @brief Get the bot user ID
   *
   * @return dpp::snowflake The bot user ID
   */
  dpp::snowflake getID() const;
  /**
   * @brief Get the Cluster ID object
   *
   * @return uint32_t The cluster ID
   */
  uint32_t getClusterID() const;
  /**
   * @brief Set the Cluster ID
   *
   * @param c The cluster ID
   */
  void setClusterID(uint32_t c);
  /**
   * @brief Get the total number of clusters
   *
   * @return uint32_t The total number of clusters
   */
  uint32_t getMaxClusters() const;

  /**
   * @brief Get the service manager instance
   *
   * @return ServiceManager*
   */
  ServiceManager *serviceManager() { return m_service_manager; }

  // Get the bot authors name (which is generated when the bot is compiled)
  std::string getName();
  // Get the bot authors email (which is generated when the bot is compiled)
  std::string getAuthor();
  // Get the bot authors version (which is generated when the bot is compiled)
  std::string getVersion();

private:
  /**
   * @brief The bot class constructor
   *
   * @param serviceManager
   */
  explicit Bot(ServiceManager *serviceManager);

  /**
   * @brief Callback method for the ready event
   *
   * @param ready Callback event information
   */
  void onReady(const dpp::ready_t &ready);

  /// Service manager instance
  ServiceManager *m_service_manager;

  /// The bot token
  std::string m_token;
  /// The cluster ID
  uint32_t m_clusterID = 0;
  /// The maximum number of clusters
  uint32_t m_maxClusters = 1;
  /// The intents
  uint32_t m_intents = dpp::intents::i_default_intents;
  /// The bot shard count
  uint32_t shard_init_count = 0;

  /// True if the bot properly started
  bool m_started = false;

  /// The bot user
  dpp::user m_user;
};
} // namespace DiscordFramework