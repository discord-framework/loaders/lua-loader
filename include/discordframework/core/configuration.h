#pragma once

#include <filesystem>
#include <nlohmann/json.hpp>

typedef nlohmann::json json;

namespace DiscordFramework {

class __attribute__((visibility("default"))) Configuration
    : public std::enable_shared_from_this<Configuration> {
public:
  /**
   * @brief Configuration object fabricator
   * @return configuration instance
   */
  static std::shared_ptr<Configuration> create(const char *path,
                                               json default_config = json({}));

  ~Configuration() = default;

  /**
   * @brief Load the configuration file
   */
  void load();

  /**
   * @brief Save the configuration file
   */
  void save();

  /**
   * @brief Get the configuration object to work with, it will give you
   * a simple `nlohmann::json` object instance
   *
   * @return json* The configuration object
   */
  json *JSON() { return &this->config; }

  /**
   * @brief Get the Env object
   *
   * @tparam T
   * @param key The environment variable name to get
   * @return T
   */
  template <typename T> T getEnv(const std::string &key);

  /**
   * @brief Check if the environment variable is set
   *
   * @param key The environment variable name to check
   * @return bool
   */
  bool hasEnv(const std::string &key);

private:
  /**
   * @brief Construct the configuration object
   */
  Configuration(const char *filePath, json default_config);

  /// The configuration file path
  std::filesystem::path config_path;
  /// The configuration file
  json config;
  /// The environment variables
  json envs;

  /**
   * @brief Set the configuration from a file stream
   *
   * @param stream Input file stream to read
   */
  void set(std::ifstream &stream);
};

template <typename T> T Configuration::getEnv(const std::string &key) {
  if (envs.contains(key)) {
    return envs.at(key).get<T>();
  } else {
    char *val = std::getenv(key.c_str());
    if (val != nullptr) {
      json v = json{{key, std::string(val)}};
      envs.merge_patch(v);
      return envs.at(key).get<T>();
    }
  }
  return nullptr;
}
} // namespace DiscordFramework
