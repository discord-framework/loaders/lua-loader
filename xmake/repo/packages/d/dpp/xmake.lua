package("dpp")
    set_homepage("https://github.com/brainboxdotcc/DPP")
    set_description("D++ Extremely Lightweight C++ Discord Library")
    set_license("Apache License 2.0")

    add_urls("https://github.com/brainboxdotcc/DPP.git")

    add_versions("v9.0.13", "a65eb9e05f17b0ffd089cf7b475d667e9b5d30cd")
    add_versions("v9.0.14", "a65eb9e05f17b0ffd089cf7b475d667e9b5d30cd")
    add_versions("v9.0.15", "a12fcf2e317c08b160dbda113ef08ea25798d381")
    add_versions("v9.0.16", "2c81e109e4558e740ebb0c13bc62993774b6fe61")

    add_deps("cmake")

    set_policy("build.merge_archive", true)

    add_deps("zlib", "openssl", "libsodium", "libopus")

    on_install(function (package)
        local configs = {}
        table.insert(configs, "-DCMAKE_BUILD_TYPE="..(package:debug() and "Debug" or "Release"))
        table.insert(configs, "-DBUILD_SHARED_LIBS="..(package:config("shared") and "ON" or "OFF"))
        table.insert(configs, "-DDPP_BUILD_TEST=OFF")
        import("package.tools.cmake").install(package, configs)
    end)
