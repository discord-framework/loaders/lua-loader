#include <discordframework/core/bot.h>
#include <discordframework/plugins-loader/loader.h>
#include <discordframework/services/logger.h>
#include <dlopen.hpp>

#include <map>
#include <memory>
#include <string>

#include "wrapper/lua.h"

using DiscordFramework::Bot;
using DiscordFramework::Services::LoggerService;

class LuaLoader : public DiscordFramework::Loader {
public:
  LuaLoader(Bot *bot);
  ~LuaLoader();

  std::string name() const override { return "lua"; }

  void load(DiscordFramework::PluginInfo *info) override;

private:
  std::map<std::string, std::shared_ptr<Lua>> m_plugins;

  std::shared_ptr<LoggerService> m_logger;
};

ENTRYPOINT(LuaLoader)
