#pragma once

#include "common.h"
#include <filesystem>

#include <discordframework/core/bot.h>
#include <discordframework/services/logger.h>
#include <functional>

using DiscordFramework::Bot;

class Lua {
public:
  Lua(Bot *bot, const std::filesystem::path &script_path,
      spdlog::logger *logger);
  ~Lua();

  void prepare();
  void run();

  std::shared_ptr<sol::state> state() { return m_state; }

  void register_destruction(std::function<void()> destruction_callback);

private:
  std::shared_ptr<sol::state> m_state;
  Bot *m_bot;
  std::filesystem::path m_script_path;

  std::vector<std::function<void()>> m_destruction_queue;

  spdlog::logger *m_logger;
};
