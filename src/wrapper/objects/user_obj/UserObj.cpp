#include "UserObj.h"

// Constructor
UserObj::UserObj(dpp::user user) { this->m_user = user; }

// Methods
bool UserObj::is_bot() const { return m_user.is_bot(); }
bool UserObj::is_system() const { return m_user.is_system(); }
bool UserObj::is_mfa_enabled() const { return m_user.is_mfa_enabled(); }
bool UserObj::is_verified() const { return m_user.is_verified(); }
bool UserObj::has_nitro_full() const { return m_user.has_nitro_full(); }
bool UserObj::has_nitro_classic() const { return m_user.has_nitro_classic(); }
bool UserObj::has_hypesquad_events() const {
  return m_user.has_hypesquad_events();
}
bool UserObj::is_verified_bot() const { return m_user.is_verified_bot(); }
bool UserObj::is_certified_moderator() const {
  return m_user.is_certified_moderator();
}
bool UserObj::is_bot_http_interactions() const {
  return m_user.is_bot_http_interactions();
}
bool UserObj::has_animated_icon() const { return m_user.has_animated_icon(); }
double UserObj::get_creation_time() const { return m_user.get_creation_time(); }

std::string UserObj::get_avatar_url(uint16_t size) const {
  return m_user.get_avatar_url(size);
}

std::string UserObj::get_mention() const { return m_user.get_mention(); }

// Attributes
std::string UserObj::get_username() { return m_user.username; }
void UserObj::set_username(std::string name) { m_user.username = name; }

uint16_t UserObj::get_discriminator() { return m_user.discriminator; }
void UserObj::set_discriminator(uint16_t discriminator) {
  m_user.discriminator = discriminator;
}

dpp::snowflake UserObj::get_id() { return m_user.id; }
void UserObj::set_id(dpp::snowflake id) { m_user.id = id; }
