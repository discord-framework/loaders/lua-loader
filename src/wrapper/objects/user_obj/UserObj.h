#pragma once

#include "../../common.h"
#include "../../lua.h"

#include <dpp/dpp.h>
#include <memory>

struct UserObj {
private:
  dpp::user m_user;

public:
  UserObj(dpp::user user);
  ~UserObj() = default;

  /*
   * methods
   */
  bool is_bot() const;
  bool is_system() const;
  bool is_mfa_enabled() const;
  bool is_verified() const;
  bool has_nitro_full() const;
  bool has_nitro_classic() const;
  bool has_hypesquad_events() const;
  bool is_verified_bot() const;
  bool is_certified_moderator() const;
  bool is_bot_http_interactions() const;
  bool has_animated_icon() const;
  double get_creation_time() const;
  std::string get_avatar_url(uint16_t size = 0) const;
  std::string get_mention() const;

  /*
   * attributes
   */
  // username
  std::string get_username();
  void set_username(std::string name);

  // discriminator
  uint16_t get_discriminator();
  void set_discriminator(uint16_t discriminator);

  // id
  dpp::snowflake get_id();
  void set_id(dpp::snowflake id);
};
