#include "BotObj.h"

void BotObj::on_ready(sol::function callback) {
  auto h =
      m_bot->cluster->on_ready([=](const dpp::ready_t &event) { callback(); });

  m_instance->register_destruction(
      [&]() { m_bot->cluster->on_ready.detach(h); });
}

void BotObj::on_message_create(sol::function callback) {
  auto h = m_bot->cluster->on_message_create(
      [=](const dpp::message_create_t &event) { callback(event.msg); });

  m_instance->register_destruction(
      [&]() { m_bot->cluster->on_message_create.detach(h); });
}
