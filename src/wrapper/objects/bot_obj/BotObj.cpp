#include "BotObj.h"

#include <mutex>

BotObj::BotObj(Lua *instance, Bot *bot) : m_bot(bot), m_instance(instance) {}

std::shared_ptr<BotObj> BotObj::instance(Lua *instance, Bot *bot) {
  static std::weak_ptr<BotObj> inst;
  static std::mutex m;

  m.lock();

  auto ret = inst.lock();
  if (!ret) {
    ret.reset(new BotObj(instance, bot));
    inst = ret;
  }

  m.unlock();
  return ret;
}

UserObj BotObj::user() const { return UserObj{m_bot->cluster->me}; }

void BotObj::debug(std::string message) {
  m_bot->cluster->log(dpp::ll_debug, fmt::format("[LuaLoader] {}", message));
}
void BotObj::info(std::string message) {
  m_bot->cluster->log(dpp::ll_info, fmt::format("[LuaLoader] {}", message));
}
void BotObj::error(std::string message) {
  m_bot->cluster->log(dpp::ll_error, fmt::format("[LuaLoader] {}", message));
}
void BotObj::warning(std::string message) {
  m_bot->cluster->log(dpp::ll_warning, fmt::format("[LuaLoader] {}", message));
}

void BotObj::send_message(dpp::snowflake channel_id, std::string message) {
  try {
    dpp::message m;
    m.channel_id = channel_id;
    m.content = message;

    m_bot->cluster->message_create(
        m, [this](const dpp::confirmation_callback_t event) {
          if (event.is_error()) {
            this->m_bot->cluster->log(dpp::ll_error,
                                      fmt::format("[LuaLoader] Error: {}",
                                                  event.get_error().message));
          }
        });
  } catch (std::invalid_argument e) {
  }
}
