#pragma once

#include "../../common.h"
#include "../../lua.h"

#include <discordframework/core/bot.h>
#include <memory>

#include "../user_obj/UserObj.h"

struct BotObj {
private:
  Bot *m_bot;
  Lua *m_instance;
  BotObj(Lua *instance, Bot *bot);

public:
  ~BotObj() = default;

  static std::shared_ptr<BotObj> instance(Lua *instance, Bot *bot);

  UserObj user() const;

  // Logger
  void debug(std::string message);
  void info(std::string message);
  void error(std::string message);
  void warning(std::string message);

  void send_message(dpp::snowflake channel_id, std::string message);

  // Event handlers
  void on_ready(sol::function callback);
  void on_message_create(sol::function callback);
};
