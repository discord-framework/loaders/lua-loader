#pragma once

#include <discordframework/core/bot.h>

#include "bot.h"
#include "user.h"

void prepare_all(Lua *l, DiscordFramework::Bot *bot) {
  prepare_user(l);
  prepare_bot(l, bot);
}
