#pragma once

#include "../lua.h"

#include "../objects/user_obj/UserObj.h"

void prepare_user(Lua *l) {
  auto user_type = l->state()->new_usertype<UserObj>(
      "User", sol::constructors<UserObj(dpp::user)>());

  user_type["is_bot"] = &UserObj::is_bot;
  user_type["is_system"] = &UserObj::is_system;
  user_type["is_mfa_enabled"] = &UserObj::is_mfa_enabled;
  user_type["is_verified"] = &UserObj::is_verified;
  user_type["has_nitro_full"] = &UserObj::has_nitro_full;
  user_type["has_nitro_classic"] = &UserObj::has_nitro_classic;
  user_type["has_hypesquad_events"] = &UserObj::has_hypesquad_events;
  user_type["is_verified_bot"] = &UserObj::is_verified_bot;
  user_type["is_certified_moderator"] = &UserObj::is_certified_moderator;
  user_type["is_bot_http_interactions"] = &UserObj::is_bot_http_interactions;
  user_type["has_animated_icon"] = &UserObj::has_animated_icon;
  user_type["get_creation_time"] = &UserObj::get_creation_time;
  user_type["get_avatar_url"] = &UserObj::get_avatar_url;
  user_type["get_mention"] = &UserObj::get_mention;

  user_type["username"] =
      sol::property(&UserObj::get_username, &UserObj::set_username);
  user_type["discriminator"] =
      sol::property(&UserObj::get_discriminator, &UserObj::set_discriminator);
  user_type["id"] = sol::property(&UserObj::get_id, &UserObj::set_id);
}
