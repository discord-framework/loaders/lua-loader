#pragma once

#include "../common.h"
#include "../lua.h"

#include <discordframework/core/bot.h>

#include "../objects/bot_obj/BotObj.h"

void prepare_bot(Lua *l, DiscordFramework::Bot *bot_instance) {
  auto bot_type = l->state()->new_usertype<BotObj>(
      // No need constructor since we want to get the current bot instance
      "DiscordBot", sol::no_constructor);

  // Static method to get the bot instance
  bot_type.set("instance", [l, bot_instance]() {
    return BotObj::instance(l, bot_instance);
  });

  // The bot user
  bot_type["user"] = sol::property(&BotObj::user);

  // TODO: remove this line
  bot_type["send_message"] = &BotObj::send_message;

  // Logger
  bot_type["info"] = &BotObj::info;
  bot_type["debug"] = &BotObj::debug;
  bot_type["warning"] = &BotObj::warning;
  bot_type["error"] = &BotObj::error;

  // Event handler
  bot_type["on_ready"] = &BotObj::on_ready;
  bot_type["on_message_create"] = &BotObj::on_message_create;
};
