#include "lua.h"

#include "preparations/all.h"

Lua::Lua(Bot *bot, const std::filesystem::path &script_path,
         spdlog::logger *logger)
    : m_bot(bot), m_script_path(script_path), m_logger(logger) {
  this->m_state = std::make_shared<sol::state>();

  this->m_state->open_libraries(sol::lib::base, sol::lib::coroutine,
                                sol::lib::string, sol::lib::io,
                                sol::lib::package, sol::lib::os, sol::lib::math,
                                sol::lib::utf8, sol::lib::table);

  this->m_state->set_function("msgh", [logger](const std::string &msg) {
    logger->error("[LuaLoader][Lua Error] {}", msg);
  });
  sol::protected_function::set_default_handler((*this->m_state)["msgh"]);
}

Lua::~Lua() {
  for (auto fn : this->m_destruction_queue) {
    fn();
  }

  this->m_state->collect_garbage();
}

void Lua::prepare() { prepare_all(this, m_bot); }

void Lua::run() {
  auto res = this->m_state->safe_script_file(m_script_path);

  if (!res.valid()) {
    m_bot->cluster->log(dpp::ll_error, "lol nope error !!!");
  }
}

void Lua::register_destruction(std::function<void()> destruction_callback) {
  this->m_destruction_queue.push_back(destruction_callback);
}
