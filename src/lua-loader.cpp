#include "lua-loader.h"

#include <discordframework/services/logger.h>

LuaLoader::LuaLoader(DiscordFramework::Bot *bot) : Loader(bot) {
  m_logger = m_bot->serviceManager()->get<LoggerService>("logger");
}

LuaLoader::~LuaLoader() {}

void LuaLoader::load(DiscordFramework::PluginInfo *info) {
  auto l = std::make_shared<Lua>(m_bot, info->path, m_logger->instance());
  l->prepare();

  l->run();

  m_plugins.insert({info->name, l});
}
